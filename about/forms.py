from django import forms
from .models import Testimoni
from django.forms import ModelForm

class TestimoniForm(ModelForm):
    class Meta:
        model = Testimoni
        fields = ['message']
        widgets = {'message': forms.Textarea(attrs={'class': 'form-control', 'id': 'post-text',  'placeholder': 'Write your testimony here...', 'type': '' , 'maxlength': 500}),}


