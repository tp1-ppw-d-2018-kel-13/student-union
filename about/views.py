from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponse
from .models import Testimoni
from .forms import TestimoniForm
import requests, json, re
import datetime

# Create your views here.

def about(request):
    response_data = {}
    testi = Testimoni.objects.all()
    form = TestimoniForm()
    return render(request, 'about.html', {'form':form, 'testi':testi})

@csrf_exempt
def testimoni(request):
	response_data = {}
	if (request.method == "POST"):
		testi_post = request.POST.get('testimonials')
		response_data['name'] = request.user.username
		name = response_data['name']
		newObj = Testimoni.objects.create(name=name, message=testi_post)
		print(newObj.time)
		time = newObj.time.strftime("%b. %d, %Y, %I:%M %p")
		response_data['msg'] = testi_post
		response_data['datetime'] = datetime
		return HttpResponse(json.dumps(response_data), content_type="application/json")
	else:
		return HttpResponse(json.dumps({"there's nothing": "unsuccessful"}), content_type="application/json")

