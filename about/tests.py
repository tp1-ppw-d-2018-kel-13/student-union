from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import Testimoni
from .forms import TestimoniForm
from .views import *
import unittest

# Create your tests here.
class TestimoniUnitTest(TestCase):
    def test_testimoni_url_is_exist(self):
        response = Client().get('/about')
        self.assertEqual(response.status_code, 200)
    
    def test_testimoni_json_url_is_exist(self):
        response = Client().get('/testimoni/')
        self.assertEqual(response.status_code, 200)

    def test_testimoni_using_to_do_list_template(self):
        response = Client().get('/about')
        self.assertTemplateUsed(response, 'about.html')

    def test_model_can_create_new_testimoni(self):
        new_testimoni = Testimoni.objects.create(message="testing testimoni")
        counting_all_testimoni = Testimoni.objects.all().count()
        self.assertEqual(counting_all_testimoni, 1)

    def test_about_using_create_form_func(self):
        found = resolve('/about')
        self.assertEqual(found.func, about)