from django.db import models
from django.utils import timezone

# Create your models here.

class Testimoni(models.Model):
    name = models.CharField(max_length=200, blank=True)
    message = models.CharField(max_length=500) 
    time = models.DateTimeField(auto_now_add=True)
