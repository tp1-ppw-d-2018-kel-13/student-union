from django.shortcuts import render
from django.http import HttpResponseRedirect
from .views import *
from .models import *
from django.urls import reverse
import requests
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
import  json
from homepage import *
from django.contrib.auth import logout

# Create your views here.
def view_login_google(request):
    return render(request, 'login-google.html')

def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('homepage'))
    # Redirect to a success page.


def printscope(backend, strategy, details, response,
        user=None, *args, **kwargs):
    usr = response.get('emails')[0].get('value')
    if UserMasuk.objects.filter(email=usr).exists():
        print(usr)
    else:
        family = response.get('name').get('familyName')
        given = response.get('name').get('givenName')
        display = response.get('displayName')
        profile = response.get('image').get('url')
        profile = profile[0:len(profile)-6]
        print(profile)
        email = usr
        userbaru = UserMasuk(familyName=family, givenName=given,
                             displayName=display,profilePic=profile,
                             email=email)
        userbaru.save()