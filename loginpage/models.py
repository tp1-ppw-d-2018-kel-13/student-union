from django.db import models

class UserMasuk(models.Model):
    familyName = models.CharField(max_length=100)
    givenName = models.CharField(max_length=100)
    displayName = models.CharField(max_length=100)
    profilePic = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
# Create your models here.

