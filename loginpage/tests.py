from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import *
from .models import *
from django.utils import timezone

class LoginPageUnitTest(TestCase):

    def test_google_login_url_is_exist(self):
        response = Client().get('/login-google/')
        self.assertEqual(response.status_code, 200)

    def test_google_login_url_func_working(self):
        found = resolve(reverse('login-google'))
        self.assertEqual(found.func,view_login_google)

    def test_google_login_has_development_text(self):
        response = Client().get('/login-google/')
        self.assertContains(response, '[STILL IN DEVELOPMENT]')




