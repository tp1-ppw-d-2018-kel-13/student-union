from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import view_landing

# Create your tests here.
class LandingPageUnitTest(TestCase):
    def test_landingpage_url_is_exist(self):
        response = Client().get('')

    def test_landingpage_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'landingpage.html')

    # def test_landingpage_using_index_func(self):
    #     found = resolve('')
    #     self.assertEqual(found.func, view_landing)