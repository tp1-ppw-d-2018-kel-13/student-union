from django.shortcuts import render

from django.http import HttpResponseRedirect

from django.urls import reverse
from django.utils import timezone





# Create your views here.


def view_signup(request):
    return render(request,'register.html')