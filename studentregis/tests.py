from django.test import TestCase
from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import resolve
from django.utils import timezone
from .views import signup, signed
from .models import Register
from .forms import RegisForm
import unittest

# Create your tests here.
class StudentRegisUnitTest(TestCase):

    def test_regis_url_is_exist(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)

    def test_signed_url_is_exist(self):
        response = Client().get('/signed/')
        self.assertEqual(response.status_code, 200)

    def test_regis_using_create_form_func(self):
        found = resolve('/signup/')
        self.assertEqual(found.func, signup)

    def test_model_can_create_new_status(self):
        usernames = Register.objects.create(username='ayuy')
        response_post = Client().post('/signup/', {'firstname' : 'Taliza', 'lastname' : 'Ayu', 'username' : 'taliza', 'date' : '2000-10-10', 'email' : 'talizaayu@gmail.com', 'password' : 'abc', 'address' : 'Fasilkom'})
        self.assertEqual(response_post.status_code, 302)
        
        counting_all_username = Register.objects.all().count()
        self.assertEqual(counting_all_username, 2)
        
    def test_form_validation_for_blank_items(self):
        form = RegisForm(data={'username' : ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['username'][0],
            'This field is required.'
        )
