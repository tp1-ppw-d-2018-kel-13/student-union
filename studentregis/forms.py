from django import forms
from django.utils import timezone

class RegisForm(forms.Form):
    error_messages = {
        'required': 'Wajib diisi!',
        'invalid': 'Input tidak valid!',
    }
    attrs = {
        'class': 'form-control',
        'type' : 'text'
    }

    email_attrs = {
        'class': 'form-control',
        'type': 'email'
    }

    pass_attrs = {
        'class': 'form-control',
        'type': 'password'
    }

    firstname = forms.CharField(label="First name:")
    lastname = forms.CharField(label="Last name:")
    date = forms.DateField(widget=forms.DateInput(attrs={'type' : 'date'}), label="Date of Birth:")
    username = forms.CharField(label="Username:", required=True, widget=forms.TextInput(attrs=attrs))
    email = forms.EmailField(label="Email:", required=True, widget=forms.EmailInput(attrs=email_attrs))
    password = forms.CharField(label="Password:", required=True, widget=forms.TextInput(attrs=pass_attrs))
    address = forms.CharField(label='Address:', widget=forms.Textarea(attrs=attrs), required=True)
