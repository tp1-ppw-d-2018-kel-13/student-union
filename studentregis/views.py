from django.shortcuts import render, redirect
from .models import Register
from .forms import RegisForm
from django.http import HttpResponse, HttpResponseRedirect
from django.db import IntegrityError


# Create your views here.    
 
def signup(request):
    form = RegisForm(request.POST or None)
    regis = Register.objects.all()
    response = {
        "regis" : regis
    }
    response['form'] = form 
    
    try:
        if(request.method == "POST" and form.is_valid()):
            firstname = request.POST.get("firstname")
            lastname = request.POST.get("lastname")
            username = request.POST.get("username")
            email = request.POST.get("email")
            password = request.POST.get("password")
            date = request.POST.get("date")
            address = request.POST.get("address")
            Register.objects.create(firstname=firstname, lastname=lastname, username=username, email=email, password=password, date=date, address=address)
            return redirect('signed')
        else:
            return render(request, 'studentregis.html', response)

    except IntegrityError as e:
        return render(request, 'studentregis.html', response)

def signed(request):
    response={}
    return render(request,'signed.html')

# response = {}
# def signup(request):
#     form = RegisForm(request.POST or None)
#     if (request.method == 'POST' and form.is_valid()):
#         response['firstname'] = request.POST['firstname'] if request.POST['firstname'] != "" else "unknown firstname"
#         response['lastname'] = request.POST['lastname'] if request.POST['lastname'] != "" else "unknown lastname"
#         response['username'] = request.POST['username'] if request.POST['username'] != "" else "unknown username"
#         response['date'] = request.POST['date']
#         response['email'] = request.POST['email'] if request.POST['email'] != "" else "unknown email"
#         response['password'] = request.POST['password'] if request.POST['password'] != "" else "unknown password"
#         response['address'] = request.POST['address'] 
#         reg = Register(firstname=response['firstname'], lastname=response['lastname'], username=response['username'], date=response['date'], email=response['email'], password=response['password'], address=response['address'])
#         reg.save()
#         return HttpResponseRedirect('/signup/')
#     else:
#         return render(request,'studentregis.html',response)

# def signed(request):
#     return render(request,'signed.html')