from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('signup/', views.signup, name='signup'),
    # path('home/', views.home, name='home'),
    path('signed/', views.signed, name='signed'),
]