from django.db import models
# from datetime import date
import datetime

# Create your models here.
class Register(models.Model):
    firstname = models.CharField(max_length=100) 
    lastname = models.CharField(max_length=100)
    date = models.DateField(default=datetime.date(1999,5,1))
    username = models.CharField(max_length=40)
    email = models.EmailField()
    password = models.CharField(max_length=20)
    address = models.CharField(max_length=200)