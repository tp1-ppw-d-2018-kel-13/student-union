from django.db import models

# Create your models here.
class entry_news(models.Model):
    title = models.CharField(max_length=150)
    body = models.TextField(max_length=1500)
    thumbnail = models.CharField(max_length=100, null=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    modified_at = models.DateTimeField(auto_now=True, editable=False)

    def __str__(self):
        return self.title

    def snippet(self):
        return self.body[:50] + "..."