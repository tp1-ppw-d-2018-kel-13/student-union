from django.test import TestCase, Client
from .models import *
from django.urls import resolve, reverse
from .views import *
from django.contrib.auth import get_user_model

# Create your tests here.
class entry_news_unit_test(TestCase):
    def setUp(self):
        return entry_news.objects.create(title="lorem", body="ipsum")

    def test_string_representation(self):
        entry = entry_news(title="My entry title")
        self.assertEqual(str(entry), entry.title)

    def test_newspage_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_newspage_news_is_valid(self):
        new_news = self.setUp()
        self.assertEqual(new_news.title, 'lorem')
        self.assertEqual(new_news.body, 'ipsum')

    def test_newspage_news_can_print(self):
        new_news = self.setUp()
        self.assertEqual('lorem', new_news.__str__())

    def test_news_detail_url_is_exist(self):
        response = Client().get('/newspage/1/')
        self.assertEqual(response.status_code, 200)