from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from .models import entry_news


# Create your views here.
def view_news(request):
    title = entry_news.objects.all()
    return render(request,'newspage.html', {'title': title})

def view_news_detail(request, id):
    title = entry_news.objects.get(id=id)
    return render(request, 'news_detail.html', {'title': title})