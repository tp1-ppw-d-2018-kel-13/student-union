"""tp1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import include
# from django.urls import include
# from django.contrib import admin
# from django.urls import path, re_path
# from django.conf import settings

# urlpatterns = [
#     path('admin/', admin.site.urls),
#     # path('', include('landingpage.urls')),
# 	# re_path('', include('placeholder.urls')),
# ]
from django.conf.urls import url, include
from django.conf.urls import include
from django.urls import re_path, path
from django.contrib import admin
from django.urls import path, re_path
from landingpage.views import *
from placeholder.views import *
from newspage.views import *
from django.conf import settings
from django.conf.urls.static import static
# from studentregis.views import studentregis as index

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('studentregis.urls')),
    path('homepage', include('homepage.urls')),
    path('', include('landingpage.urls')),
	re_path('', include('placeholder.urls')),

    path('newspage/',view_news,name='news'),
    path('newspage/<int:id>/',view_news_detail,name='news_detail'),
    path('',include(('eventpage.urls'))),
    path('',include('attendantregis.urls')),
    path('',include('loginpage.urls')),
    url(r'^auth/', include('social_django.urls', namespace='social')),  # <- Here
    path('', include('about.urls')),
    path('',include(('profilepage.urls'))),
]
