from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *

# Create your tests here.
class HomepageUnitTest(TestCase):
    def test_homepage_url_is_exist(self):
        response = Client().get('/homepage')
        self.assertEqual(response.status_code,200)

    def test_homepage_template(self):
        response = Client().get('/homepage')
        self.assertTemplateUsed(response, 'homepage.html')
    
    def test_homepage_using_index_func(self):
        found = resolve('/homepage')
        self.assertEqual(found.func, homepage)