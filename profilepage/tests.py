from django.test import TestCase
from django.http import HttpRequest
from django.urls import resolve
from django.test import Client
from .views import *

# Create your tests here.
class ProfilePageTest(TestCase):
    def test_profile_page_status_code(self):
        response = Client().get('/profile/')
        self.assertEquals(response.status_code, 200)

    def test_profile_page_is_completed(self):
        response = Client().get("/profile/")
        html_response = response.content.decode('utf8')
        self.assertIn("events", html_response)