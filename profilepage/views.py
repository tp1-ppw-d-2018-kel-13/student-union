from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.core import serializers
from eventpage.models import NewEvent
from loginpage.models import UserMasuk
import json
# comment
# Create your views here.
# comment
def profilepage(request):
    return render(request, "profilepage.html")

def get_events(request):
    usr = request.user.email
    cur_UserMasuk = UserMasuk.objects.get(email=usr)
    get_for_html = cur_UserMasuk.daftar_pendaftar.all()
    data = serializers.serialize("json", cur_UserMasuk.daftar_pendaftar.all())
    json_now = json.dumps(json.loads(data))
    return HttpResponse(data, content_type='application/json')
