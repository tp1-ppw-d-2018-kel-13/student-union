# Generated by Django 2.1.1 on 2018-12-10 10:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('loginpage', '0001_initial'),
        ('eventpage', '0010_auto_20181206_0113'),
    ]

    operations = [
        migrations.CreateModel(
            name='NewEvent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('insight', models.TextField()),
                ('date', models.DateField()),
                ('contact', models.CharField(max_length=64)),
                ('thumbnail', models.CharField(max_length=100, null=True)),
                ('listOfUser', models.ManyToManyField(blank=True, related_name='daftar_pendaftar', to='loginpage.UserMasuk')),
            ],
        ),
    ]
