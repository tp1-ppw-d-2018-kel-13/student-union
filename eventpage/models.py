from django.db import models
from loginpage.models import *

# Create your models here.
class EventAttendant(models.Model):
    username = models.CharField(max_length=40)
    password = models.CharField(max_length=20)
    email = models.EmailField()

class NewEvent(models.Model):
    name = models.CharField(max_length=100)
    insight = models.TextField()
    date = models.DateField()
    contact = models.CharField(max_length=64)
    thumbnail = models.CharField(max_length=100, null=True)
    listOfUser = models.ManyToManyField(UserMasuk,
                                             related_name='daftar_pendaftar', blank=True)


class Event(models.Model):
    name = models.CharField(max_length=100)
    insight = models.TextField()
    date =  models.DateField()
    contact = models.CharField(max_length=64)
    thumbnail = models.CharField(max_length=100, null=True)
    listOfAttendant = models.ManyToManyField(EventAttendant,
                                             related_name='registered_events',blank=True)

    def __str__(self):
        return self.name