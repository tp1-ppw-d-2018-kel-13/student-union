from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import *
from .models import *
from django.utils import timezone


# Create your tests here...

class EventPageUnitTest(TestCase):
    def setUp(self):
        return NewEvent.objects.create(name="lorem", insight="ipsum"
                                    ,date=timezone.now(),contact="dolor",
                                    thumbnail="https://files.catbox.moe/12qvlj.jpg",
                                    )

    def setUp1(self):
        return UserMasuk.objects.create(username="fulan", password="rezero1",
                                             email="kelompok13ppwd@gmail.com")

    def test_eventpage_url_is_exist(self):
        response = Client().get('/events/')
        self.assertEqual(response.status_code, 200)

    def test_eventpage_url_func_working(self):
        found = resolve(reverse('events'))
        self.assertEqual(found.func,view_events)

    def test_eventpage_contains_correct_html(self):
        response = self.client.get('/events/')
        self.assertContains(response, 'DATE &amp; TIME')
        self.assertContains(response, 'CONTACT')
        self.assertContains(response, 'EVENT ATTENDANT')

    def test_eventpage_event_is_valid(self):
        new_event = self.setUp()
        boi = new_event.date
        self.assertEqual(new_event.name, 'lorem')
        self.assertEqual(new_event.insight, 'ipsum')
        self.assertEqual(new_event.date, boi)
        self.assertEqual(new_event.contact,'dolor')
        self.assertEqual(new_event.thumbnail, "https://files.catbox.moe/12qvlj.jpg")

    def test_eventpage_event_can_print(self):
        new_event = self.setUp()
        self.assertEqual('lorem', new_event.name)

  






