from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import *
from .forms import *

# Create your views here.
response = {}
def view_events(request):
    events = NewEvent.objects.all()
    response['event_display'] = events
    return render(request, 'eventpage.html', response)



def view_regist(request,id):
    event = NewEvent.objects.get(id=id)
    return render(request, 'regis.html', {'cur_event' : event})

def regis_post(request,id) :
    usr = request.user.email
    auth = request.user.is_authenticated
    checkadm = request.user.username == "admin"
    cur_event = NewEvent.objects.get(id=id)
    check = cur_event.listOfUser.filter(email=usr).exists()
    print(check)

    UserMasuk.objects.filter(email=usr).exists()
    if auth and (not checkadm) and (not check) :
        cur_UserMasuk = UserMasuk.objects.get(email=usr)
        cur_event.listOfUser.add(cur_UserMasuk)
        return HttpResponseRedirect('/events/')
    else:
        return HttpResponseRedirect('/homepage/')
