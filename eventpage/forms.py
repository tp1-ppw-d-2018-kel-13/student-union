from django import forms

class Regist_Form(forms.Form):
    error_messages = {
        'required': 'Wajib diisi!',
        'invalid': 'Input tidak valid!',
    }
    attrs = {
        'class': 'form-control',
        'type' : 'text'
    }

    email_attrs = {
        'class': 'form-control',
        'type': 'email'
    }

    pass_attrs = {
        'class': 'form-control',
        'type': 'password'
    }

    username = forms.CharField(label='username', required=True,
                           widget=forms.TextInput(attrs=attrs))
    password = forms.CharField(label='password', required=True,
                           widget=forms.TextInput(attrs=pass_attrs))

    email = forms.EmailField(label='email', required=True, widget=forms.EmailInput(attrs=email_attrs))
